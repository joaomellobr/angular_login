import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

export  const anim1 = trigger('screenAppeared', [
  state('ready', style({opacity: 1})),
  transition('void => ready', [
    style({opacity: 0, transform: 'translate(-180px, -50px)'}),
    animate('500ms 0s ease-in-out')
  ])
]);

export  const anim2 = trigger('screenAppeared', [
  state('ready', style({opacity: 1})),
  transition('void => ready', [
    style({opacity: 0, transform: 'translate(180px, 50px)'}),
    animate('500ms 0s ease-in-out')
  ])
]);



